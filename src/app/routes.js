//NOTAA: gracias al gulp no tenemos que importar el archivo (js) por ejemplo routes.js
//la cabecera del router
angular.module('practica').config(routeConfig);
//llamamos a la funcion routeConfig
function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  //declaramos todos los parametros
  $locationProvider.html5Mode(true);
  //declaramos el enrutamiento
  $urlRouterProvider.otherwise('/web');
  /*esto proviene del ui-router y tiene el metodo que recibe 2 parametros:
  cadena con el nombre del estado
  defininion de la url*/
  $stateProvider
    //aqui defininimos en nombre del estado o en estado padre
    .state('app',{
      //definicion el nombre de la url
      url:'/web',
      //definicion lo que son vistas o visualizaciones o datos estaticos en el index
      views:{
        //visualizamos el navegador
        'navegador@':{
          templateUrl:'app/navbar/nav.html'
        },
        //visualizaremos el menu en el index
        'menu@':{
          templateUrl:'app/menu/menu.html'
          //visualizamos el contacto
        },
        'contacto@':{
          templateUrl:'app/contactos/contacto.html'
        },
        //visualizamos pie de pagina
        'pie@':{
          templateUrl:'app/footer/footer.html'
        }
      }
    })

    //ahora creamos nuestros stados dinamicos en el area de trabajo
    //el app-->proviene de estaado padre / productos-->el nombre del estado hijo
    //de tal modo redireccionaremos
    .state('app.productos',{
      //creamos la url del stado hijo
      url:'/productos',
      //visualizamos en la pantalla
      views:{
        //colocamos en nombre de la vista
        'at@':{
          //direccion con templateUrl a la carpeta y archivo  correspondiente
          templateUrl:'app/productos/producto.html',
          //definimos el controlador del producto que poviene del archivo productoCtrl
          controller:'ProductoCtrl'
        }
      }
    })

    .state('app.cliente',{
      //creamos la url del stado hijo
      url:'/clientes',
      //visualizamos en la pantalla
      views:{
        //colocamos en nombre de la vista
        'at@':{
          //direccion con templateUrl a la carpeta y archivo  correspondiente
          templateUrl:'app/clientes/cliente.html',
          //definimos el controlador del producto que poviene del archivo productoCtrl
        controller:'ClienteCtrl'
        }
      }
    })
    .state('app.facturas', {
        resolve: {
          ps: function($http) {
            return $http.get('/api/productos').
            then(function(resp) {
            //  alert('productos')
              return resp.data;
            });
          }
        },
        url: '/facturas',
        views: {
          'at@': {
            templateUrl: 'app/factura/plantilla.html',
            controller: 'FacturaCtrl'
          },
        }
      })
      .state('app.usuario',{
      resolve: {
        ps: function($http) {
          return $http.get('/api/usuarios').
          then(function(resp) {
          //  alert('productos')
            return resp.data;
          });
        }
      },
      url: '/usuarios',
      views: {
        'at@': {
          templateUrl: 'app/usuario/usuario.html',
          controller: 'UsuarioCtrl'
        },
      }
    })

  /*  .state('app.usuario',{
      //creamos la url del stado hijo
      url:'/usuario',
      //visualizamos en la pantalla
      views:{
        //colocamos en nombre de la vista
        'at@':{
          //direccion con templateUrl a la carpeta y archivo  correspondiente
          templateUrl:'app/usuario/form.html',
          //definimos el controlador del producto que poviene del archivo productoCtrl
          //controller:'UsuarioFormCtrl'
        }
      }
    })*/
}
