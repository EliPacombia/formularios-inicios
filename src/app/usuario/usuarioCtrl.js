//la cebeza de cualquier controlador o archivo js
angular.module('practica')
  //definimos el nombre del controlador
  .controller('UsuarioCtrl', function(
    //seleccionado la funciones que vamos utilizar
    $scope, $http, $uibModal, ps,defaultErrorMessageResolver) {

      defaultErrorMessageResolver.setI18nFileRootPath('bower_components/angular-auto-validate/dist/lang');
      defaultErrorMessageResolver.setCulture('es-CO');

      //el scope trabaja como un puntero, pero hace ese puntero le asignamos el valor de ps
    $scope.usuarios = ps;
    /*
    $http.get('/api/productos').
    then(function(resp) {
      alert('productos')
      $scope.productos = resp.data;
    });
    */
    //  alert('ctrl')


//datos solo para mostrar el frontend
    $scope.data = {
      a: 'usuario',
      b: 'fff'
    }
//en esta funcion abrimos el modal para creamos nuevo usaurio
    $scope.crear = function() {
      //abrimos el modal
      $uibModal.open({
        //direccionamos el archivo
        templateUrl: 'app/usuario/form.html',
        //asifanamos el controalador del formulario
        controller: 'usuarioFormCtrl',
        //para el tamaño
        size: 'lg'
      });

    }

    //$http.get('http://localhost:8080/api/productos').
    //$http.get('http://localhost:3000/api/productos') proxy: sabe como resolver /api/**

  });
