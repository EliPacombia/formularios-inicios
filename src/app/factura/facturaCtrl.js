//la cebeza de cualquier controlador o archivo js
angular.module('practica')
  //definimos el nombre del controlador
  .controller('FacturaCtrl', function(
    //seleccionado la funciones que vamos utilizar
    $scope, $http, $uibModal, ps) {
      //el scope trabaja como un puntero, pero hace ese puntero le asignamos el valor de ps
    $scope.productos = ps;
    /*
    $http.get('/api/productos').
    then(function(resp) {
      alert('productos')
      $scope.productos = resp.data;
    });
    */
    //  alert('ctrl')



    $scope.data = {
      a: 'dd',
      b: 'fff'
    }

    $scope.crear = function() {
      $uibModal.open({
        templateUrl: 'app/factura/formulario.html',
        controller: 'FacturaFormCtrl',
        size: 'lg'
      });

    }

    //$http.get('http://localhost:8080/api/productos').
    //$http.get('http://localhost:3000/api/productos') proxy: sabe como resolver /api/**

  });
