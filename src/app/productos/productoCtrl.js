//cabecera del controlador del producto
//NOTA: este controlador no se define por nombre del archivo si no por el nombre del controllador
angular.module('practica').controller('ProductoCtrl', function($scope, $http) {
  //Ingremos los  datos directamente de los productos
  $scope.productos = [{
      nombre: 'Laptop',
      precio: 2000
    },
    {
      nombre: 'Teclado',
      precio: 200
    },
    {
      nombre: 'Monitor',
      precio: 304
    }
  ];
//ingresamos los datos del producto
  $scope.producto = {
    nombre: 'Televisor',
    fecha: new Date(),
    precio: 456
  };
// direcionamos a la base de datos al cual queremos enviar
  $http.get('http://localhost:9090/api/productos')
  //callback o llamado
    .then(function(resp) {
      $scope.productos = resp.data;
    });
    //agregamos la funcion guardar
  $scope.guardar = function(form) {
    if (form.$valid) {
      //$scope.productos.push($scope.producto);
      //$scope.producto = {};
      //agregamos el metodo en el cual guardaremos nuestros productos
      $http({
        //direccion de la base de datos
        url: 'http://localhost:9090/api/productos',
        //el metodo que usaremos para guardar en la base de datos
        method: 'POST',
        data: angular.toJson($scope.producto),
        //la cabecera del metodo post lo sacamos del postman o cualquira ue se pueda probar el lado del backend
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
        // callback
      }).then(function(resp) {

        alert('Guardado Satisfactoriamente');
      })
    }else {
      alert('Formulario invalid')
    }
  }
  //creamos la funcion editar , en este caso ira con un parametro
  $scope.editar = function(producto) {
    $scope.producto = producto;
  }
});
